Функционал:
загрузка списка друзей пользователя ВКонтакте по ID пользователя.

Лэйаут:
верхний блок (текстовое поле + кнопка "Загрузить"), под ним - таблица.

Что делаем:
1) пользователь вбивает свой ID ВКонтакте и нажимает кнопку "Список друзей"
2) приложение асинхронно загружает список имён друзей и показывает их в таблице
3) при выборе имени пользователя в списке приложение загружает и показывает список друзей выбранного пользователя (в той же таблице, список заменяется)

Важно:
1) приложение должно показывать в UI, что идёт загрузка данных (прогресс не нужно, достаточно просто индикации).
2) индикация должна отключаться по окончании загрузки
3) для индикации нельзя использовать [UIApplication setNetworkActivityIndicatorVisible]
4) приложение должно быть реализовано без сторонних фреймворков, не входящих в iOS SDK.
5) приложение должно поддерживать версии iOS 6 и выше

Примечание.
Получение списка друзей -- часть публичного API Вконтакте, не требующего access token.
В качестве примера в текстовое поле можно ввести ID Павла Дурова (ID = 1) 
Документация здесь: http://vk.com/dev/friends.get