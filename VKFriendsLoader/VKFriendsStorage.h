//
//  VKFriendsStorage.h
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKFriendsStorage : NSObject

@property (nonatomic, readonly) NSArray *friends;

- (void)loadFriendsForUser:(NSNumber *)vkUserId;

@end
