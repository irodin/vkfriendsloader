//
//  VKUser.h
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKUser : NSObject

@property (nonatomic, readonly) NSNumber *vkId;
@property (nonatomic, readonly) NSString *fullName;

- (id)initWithJSON:(NSDictionary *)dict;

@end
