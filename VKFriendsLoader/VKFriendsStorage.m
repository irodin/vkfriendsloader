//
//  VKFriendsStorage.m
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "VKFriendsStorage.h"
#import "AppConstants.h"
#import "VKUser.h"
#import "VKAPIManager.h"

@implementation VKFriendsStorage

- (id)init
{
    if (self = [super init]) {
        _friends = @[];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendsDidLoad:) name:VKFriendsDidLoadNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadFriendsForUser:(NSNumber *)vkUserId {
    _friends = @[];
    [[VKAPIManager sharedInstance] requestFriendsForUser:vkUserId];
}

- (void)onFriendsDidLoad:(NSNotification *)aNotification
{
    NSMutableArray *friends = [NSMutableArray new];
    NSDictionary *friendsJSON = [(NSDictionary *)aNotification.object objectForKey:@"response"];
    for (NSDictionary *friendJSON in friendsJSON) {
        VKUser *aFriend = [[VKUser alloc] initWithJSON:friendJSON];
        [friends addObject:aFriend];
    }
    _friends = friends;

    [[NSNotificationCenter defaultCenter] postNotificationName:VKFriendsDidUpdateNotification object:nil];
}

@end
