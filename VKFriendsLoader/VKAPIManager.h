//
//  VKAPIManager.h
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKAPIManager : NSObject

+ (VKAPIManager *)sharedInstance;

- (void)requestFriendsForUser:(NSNumber *)vkUserId;

@end
