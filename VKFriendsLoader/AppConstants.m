//
//  AppConstants.m
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "AppConstants.h"

NSString *const VKFriendsDidLoadNotification = @"VKFriendsDidLoadNotification";
NSString *const VKFriendsDidFailToLoadNotification = @"VKFriendsDidFailToLoadNotification";
NSString *const VKFriendsDidUpdateNotification = @"VKFriendsDidUpdateNotification";