//
//  AppConstants.h
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const VKFriendsDidLoadNotification;
FOUNDATION_EXPORT NSString *const VKFriendsDidFailToLoadNotification;
FOUNDATION_EXPORT NSString *const VKFriendsDidUpdateNotification;