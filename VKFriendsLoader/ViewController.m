//
//  ViewController.m
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "ViewController.h"
#import "VKFriendsStorage.h"
#import "VKUser.h"
#import "AppConstants.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    __weak IBOutlet UITextField *_vkUserIdTextField;
    __weak IBOutlet UITableView *_vkFriendsTableView;
    __weak IBOutlet UIActivityIndicatorView *_aiView;
    
    VKFriendsStorage *_vkFriendsStorage;
}

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendsDidUpdate:) name:VKFriendsDidUpdateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendsDidUpdate:) name:VKFriendsDidFailToLoadNotification object:nil];
    
    _vkFriendsStorage = [VKFriendsStorage new];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (IBAction)onLoadFriends:(id)sender
{
    [self loadFriendsForCurrentUserId];
}

- (void)loadFriendsForCurrentUserId
{
    [self.view endEditing:YES];
    
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setAllowsFloats:NO];
    NSNumber *vkUserId = [nf numberFromString:_vkUserIdTextField.text];
    
    if (!vkUserId) {
        [[[UIAlertView alloc]
          initWithTitle:@"Error"
          message:@"You must specify a valid user id (a number)!"
          delegate:nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil] show];
        return;
    }
    
    [_aiView startAnimating];
    _vkFriendsTableView.userInteractionEnabled = NO;
    
    [_vkFriendsStorage loadFriendsForUser:vkUserId];
}

- (void)onFriendsDidUpdate:(NSNotification *)aNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_aiView stopAnimating];
        _vkFriendsTableView.userInteractionEnabled = YES;
        [_vkFriendsTableView reloadData];
    });
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _vkFriendsStorage.friends.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VKUser *aFriend = [_vkFriendsStorage.friends objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.textLabel.text = aFriend.fullName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VKUser *aFriend = [_vkFriendsStorage.friends objectAtIndex:indexPath.row];
    _vkUserIdTextField.text = [aFriend.vkId stringValue];
    [self loadFriendsForCurrentUserId];
}

@end
