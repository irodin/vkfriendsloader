//
//  VKAPIManager.m
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "VKAPIManager.h"
#import "SynthesizeSingleton.h"
#import "AppConstants.h"

#define NETWORK_DEFAULT_ENCODING (NSUTF8StringEncoding)

@interface VKAPIManager () {
    NSOperationQueue *_callbackQueue;
}

@end

@implementation VKAPIManager

SYNTHESIZE_SINGLETON_FOR_CLASS(VKAPIManager)

- (id)init
{
    if (self = [super init]) {
        _callbackQueue = [NSOperationQueue new];
    }
    
    return self;
}

- (void)requestFriendsForUser:(NSNumber *)vkUserId
{
    NSURL *baseUrl = [NSURL URLWithString:@"https://api.vk.com/method/friends.get"];
    NSDictionary *params = @{
        @"user_id" : [vkUserId stringValue],
        @"fields"  : @"nickname"
    };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:baseUrl];
    request.HTTPMethod = @"GET";
    
    // Append params to base URL
    NSMutableString *fullUrlString = [[NSMutableString alloc] initWithString:[baseUrl absoluteString]];
    BOOL hasParams = NO;
    for (NSString *paramName in params) {
        if (!hasParams) {
            [fullUrlString appendString:@"?"];
            hasParams = YES;
        } else {
            [fullUrlString appendString:@"&"];
        }
        NSString *paramValue = [[params objectForKey:paramName] stringByAddingPercentEscapesUsingEncoding:NETWORK_DEFAULT_ENCODING];
        [fullUrlString appendFormat:@"%@=%@", paramName, paramValue];
    }
    
    [request setURL:[NSURL URLWithString:fullUrlString]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:_callbackQueue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *receivedData,
                                               NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc]
                  initWithTitle:@"Error"
                  message:[error localizedDescription]
                  delegate:nil
                  cancelButtonTitle:@"OK"
                  otherButtonTitles:nil] show];
            });
            [[NSNotificationCenter defaultCenter] postNotificationName:VKFriendsDidFailToLoadNotification object:nil userInfo:nil];
        } else {
            NSError *parsingError;
            id receivedJSON = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers error:&parsingError];
            if (parsingError) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc]
                      initWithTitle:@"Error"
                      message:[parsingError localizedDescription]
                      delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
                });
                [[NSNotificationCenter defaultCenter] postNotificationName:VKFriendsDidFailToLoadNotification object:nil userInfo:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:VKFriendsDidLoadNotification object:receivedJSON];
            }
        }
     }];
}

@end
