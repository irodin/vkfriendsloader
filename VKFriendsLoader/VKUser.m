//
//  VKUser.m
//  VKFriendsLoader
//
//  Created by admin on 28.10.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "VKUser.h"

@implementation VKUser

- (id)initWithJSON:(NSDictionary *)dict
{
    if (self = [super init]) {
        _vkId = [dict objectForKey:@"user_id"];
        
        NSString *firstName = [dict objectForKey:@"first_name"];
        NSString *lastName = [dict objectForKey:@"last_name"];
        _fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    }
    return self;
}

@end
